# README #

This script can be used to monitor load, memory status on the server. It can send alert to telegram.

### How do I get set up? ###

First create a telegram bot and initiate a conversation with the bot. Add Token ID and chat ID in the file loadmonitor.sh.
Telegram API documentaion: https://core.telegram.org/bots

The script can be configured to start at boot by adding it to cron.

```
@reboot /path/to/script/files
```