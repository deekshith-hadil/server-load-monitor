#!/bin/bash
#This script monitors server load and memory usage for every 10 seconds and sends notification to telegram in case of high usage.

chat_id="xxxxxxx"	#telegram chat ID
token="xxxx:xxxxxxxxxxxxxxxxx" #telegram bot ID

#Temporary files to store data
resource_usage_info=/tmp/resource_usage_info.txt
msg_caption=/tmp/telegram_msg_caption.txt

#Set threshold levels for memory usage and load here. If the usage exceeds these values, the notification will be sent.
mem_threshold=80 #Should be interger. This is in percentage
load_threshold=$(nproc) #Should be integer. Usually total number of cores.

#Telegram API to send notificaiton.
function telegram_send
{
curl -s -F chat_id=$chat_id -F document=@$resource_usage_info -F caption="$caption" https://api.telegram.org/bot$token/sendDocument > /dev/null 2&>1
rm -f $resource_usage_info
rm -f $msg_caption
}

function telegram_recover_alert
{
text_msg=$(echo -e "$res_status recovered\n$res_info")
curl -s -F chat_id=$chat_id -F text="$text_msg" https://api.telegram.org/bot$token/sendMessage > /dev/null 2&>1
}

#Monitoring Load on the server
while :
do
    CPU_USAGE=$(top -b -d1 -n1|grep -i "Cpu(s)")
    mem=$(free -m)
    min_load=$(cat /proc/loadavg | cut -d . -f1)
    if [ $min_load -ge $load_threshold ]
        then
        echo -e "High CPU usage detected on $(hostname)\n$(uptime)" > $msg_caption
        echo -e "CPU usage report from $(hostname)\nServer Time : $(date +"%d%b%Y %T")\n\n\$uptime\n$(uptime)\n\nCPU USAGE:\n$CPU_USAGE\n\n\$free -m output\n$mem\n\n%CPU %MEM RSS\tUSER\tCMD" >         $resource_usage_info
        ps -eo pcpu,pmem,rss,user,cmd | sed '1d' | sort -nr >> $resource_usage_info 
        caption=$(<$msg_caption)
        telegram_send
        count=0
        while [ $count -lt 30 ]
        do
            count=$(($count + 1))
            sleep 30
            cur_load=$(cat /proc/loadavg | cut -d . -f1)
            if [ $cur_load -lt $load_threshold ]
            then
                res_info=$(uptime)
                res_status="load"
                telegram_recover_alert
                break
            fi
        done
    fi
    sleep 10


#Monitoring Memory usage on the server
    mem=$(free -m)
    mem_usage=$(echo "$mem" | awk 'NR==2{printf "%i\n", ($3*100/$2)}')
    if [ $mem_usage -gt $mem_threshold ]
    then
        echo -e "High Memory usage detected on $(hostname)\n$(echo $mem_usage% memory usage)" > $msg_caption
        echo -e "Memory consumption Report from $(hostname)\nServer Time : $(date +"%d%b%Y %T")\n\nCPU_USAGE:\n$CPU_USAGE\n\n\$free -m output\n$mem\n\n%MEM %CPU RSS USER\tCMD" > $resource_usage_info
        ps -eo pmem,pcpu,rss,user,cmd | sed '1d' | sort -nr >> $resource_usage_info
        caption=$(<$msg_caption)
        telegram_send
        count=0
        while [ $count -lt 30 ]
        do
            count=$(($count + 1))
            sleep 30
            cur_mem_usage=$(free -m | awk 'NR==2{printf "%i\n", ($3*100/$2)}')
            if [ $cur_mem_usage -lt $mem_threshold ]
            then
                res_info="memory usage = $cur_mem_usage%"
                res_status="memory usage"
                telegram_recover_alert
                break
            fi
        done
    fi 
    sleep 10
done
